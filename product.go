package go_shopify

import (
    "fmt"
    "regexp"
)

const productsBasePath = "products"
const productsResourceName = "products"

// linkRegex is used to extract pagination links from product search results.
var linkRegex = regexp.MustCompile(`^ *<([^>]+)>; rel="(previous|next)" *$`)

type ProductService interface {
    List(interface{}) ([]Product, error)
}
type Product struct {
    ID          int64     `json:"id,omitempty"`
    Title       string    `json:"title,omitempty"`
    BodyHTML    string    `json:"body_html"`
    Vendor      string    `json:"vendor,omitempty"`
    ProductType string    `json:"product_type"`
    Status      string    `json:"status,omitempty"`
    Variants    []Variant `json:"variants,omitempty"`
}
type ProductServiceOp struct {
    client *Client
}
type ProductsResource struct {
    Products []Product `json:"products"`
}

func (s *ProductServiceOp) List(options interface{}) ([]Product, error) {
    path := fmt.Sprintf("%s.json", productsBasePath)
    resource := new(ProductsResource)
    err := s.client.Get(path, resource, options)
    return resource.Products, err
}
