package go_shopify

import "github.com/shopspring/decimal"

type PriceSet struct {
    ShopMoney        PriceSetMoney `json:"shop_money,omitempty"`
    PresentmentMoney PriceSetMoney `json:"presentment_money,omitempty"`
}

type PriceSetMoney struct {
    Amount       *decimal.Decimal `json:"amount,omitempty"`
    CurrencyCode string           `json:"currency_code,omitempty"`
}
