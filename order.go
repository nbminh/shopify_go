package go_shopify

import (
	"fmt"
	"github.com/shopspring/decimal"
	"time"
)

const (
	ordersBasePath = "orders"
)

type OrderServiceOp struct {
	client *Client
}

type OrderResource struct {
	Order *Order `json:"order"`
}

type OrderService interface {
	Create(Order) (*Order, error)
	Get(int64, interface{}) (*Order, error)
}

type Order struct {
	ID                    int64                 `json:"id,omitempty"`
	Name                  string                `json:"name,omitempty"`
	Email                 string                `json:"email,omitempty"`
	CreatedAt             *time.Time            `json:"created_at,omitempty"`
	UpdatedAt             *time.Time            `json:"updated_at,omitempty"`
	CancelledAt           *time.Time            `json:"cancelled_at,omitempty"`
	ClosedAt              *time.Time            `json:"closed_at,omitempty"`
	ProcessedAt           *time.Time            `json:"processed_at,omitempty"`
	Customer              *CustomerOrder        `json:"customer,omitempty"`
	BillingAddress        *Address              `json:"billing_address,omitempty"`
	ShippingAddress       *Address              `json:"shipping_address,omitempty"`
	Currency              string                `json:"currency,omitempty"`
	PresentmentCurrency   string                `json:"presentment_currency,omitempty"`
	TotalPrice            *decimal.Decimal      `json:"total_price,omitempty"`
	TotalPriceSet         *PriceSet             `json:"total_price_set,omitempty"`
	TotalShippingPriceSet *PriceSet             `json:"total_shipping_price_set,omitempty"`
	SubtotalPrice         *decimal.Decimal      `json:"subtotal_price,omitempty"`
	TotalDiscounts        *decimal.Decimal      `json:"total_discounts,omitempty"`
	TotalLineItemsPrice   *decimal.Decimal      `json:"total_line_items_price,omitempty"`
	TaxesIncluded         bool                  `json:"taxes_included,omitempty"`
	TotalTax              *decimal.Decimal      `json:"total_tax,omitempty"`
	TaxLines              []TaxLine             `json:"tax_lines,omitempty"`
	TotalWeight           int                   `json:"total_weight,omitempty"`
	FinancialStatus       string                `json:"financial_status,omitempty"`
	Fulfillments          []Fulfillment         `json:"fulfillments,omitempty"`
	FulfillmentStatus     string                `json:"fulfillment_status,omitempty"`
	Token                 string                `json:"token,omitempty"`
	CartToken             string                `json:"cart_token,omitempty"`
	Number                int                   `json:"number,omitempty"`
	OrderNumber           int                   `json:"order_number,omitempty"`
	Note                  string                `json:"note,omitempty"`
	Test                  bool                  `json:"test,omitempty"`
	BrowserIp             string                `json:"browser_ip,omitempty"`
	BuyerAcceptsMarketing bool                  `json:"buyer_accepts_marketing,omitempty"`
	CancelReason          string                `json:"cancel_reason,omitempty"`
	NoteAttributes        []NoteAttribute       `json:"note_attributes,omitempty"`
	DiscountCodes         []DiscountCode        `json:"discount_codes,omitempty"`
	LineItems             []LineItem            `json:"line_items,omitempty"`
	ShippingLines         []ShippingLines       `json:"shipping_lines,omitempty"`
	Transactions          []Transaction         `json:"transactions,omitempty"`
	AppID                 int                   `json:"app_id,omitempty"`
	CustomerLocale        string                `json:"customer_locale,omitempty"`
	LandingSite           string                `json:"landing_site,omitempty"`
	ReferringSite         string                `json:"referring_site,omitempty"`
	SourceName            string                `json:"source_name,omitempty"`
	Tags                  string                `json:"tags,omitempty"`
	LocationId            int64                 `json:"location_id,omitempty"`
	PaymentGatewayNames   []string              `json:"payment_gateway_names,omitempty"`
	ProcessingMethod      string                `json:"processing_method,omitempty"`
	Refunds               []Refund              `json:"refunds,omitempty"`
	UserId                int64                 `json:"user_id,omitempty"`
	OrderStatusUrl        string                `json:"order_status_url,omitempty"`
	Gateway               string                `json:"gateway,omitempty"`
	Confirmed             bool                  `json:"confirmed,omitempty"`
	CheckoutToken         string                `json:"checkout_token,omitempty"`
	Reference             string                `json:"reference,omitempty"`
	SourceIdentifier      string                `json:"source_identifier,omitempty"`
	SourceURL             string                `json:"source_url,omitempty"`
	DeviceID              int64                 `json:"device_id,omitempty"`
	Phone                 string                `json:"phone,omitempty"`
	LandingSiteRef        string                `json:"landing_site_ref,omitempty"`
	CheckoutID            int64                 `json:"checkout_id,omitempty"`
	ContactEmail          string                `json:"contact_email,omitempty"`
	DiscountApplications  []DiscountApplication `json:"discount_applications,omitempty"`
	PaymentDetails        []string              `json:"payment_details,omitempty"`
	InventoryBehaviour    string                `json:"inventory_behaviour,omitempty"`
	SendReceipt           bool                  `json:"send_receipt,omitempty"`
}

type TaxLine struct {
	Title string           `json:"title,omitempty"`
	Price *decimal.Decimal `json:"price,omitempty"`
	Rate  *decimal.Decimal `json:"rate,omitempty"`
}

type NoteAttribute struct {
	Name  string      `json:"name,omitempty"`
	Value interface{} `json:"value,omitempty"`
}

type DiscountCode struct {
	Amount *decimal.Decimal `json:"amount,omitempty"`
	Code   string           `json:"code,omitempty"`
	Type   string           `json:"type,omitempty"`
}

type LineItem struct {
	ID                         int64                `json:"id,omitempty"`
	ProductID                  int64                `json:"product_id,omitempty"`
	VariantID                  int64                `json:"variant_id,omitempty"`
	Quantity                   int                  `json:"quantity,omitempty"`
	Price                      *decimal.Decimal     `json:"price,omitempty"`
	TotalDiscount              *decimal.Decimal     `json:"total_discount,omitempty"`
	Title                      string               `json:"title,omitempty"`
	VariantTitle               string               `json:"variant_title,omitempty"`
	Name                       string               `json:"name,omitempty"`
	SKU                        string               `json:"sku,omitempty"`
	Vendor                     string               `json:"vendor,omitempty"`
	GiftCard                   bool                 `json:"gift_card,omitempty"`
	Taxable                    bool                 `json:"taxable,omitempty"`
	RequiresShipping           bool                 `json:"requires_shipping,omitempty"`
	VariantInventoryManagement string               `json:"variant_inventory_management,omitempty"`
	PreTaxPrice                *decimal.Decimal     `json:"pre_tax_price,omitempty"`
	Properties                 []NoteAttribute      `json:"properties,omitempty"`
	ProductExists              bool                 `json:"product_exists,omitempty"`
	FulfillableQuantity        int                  `json:"fulfillable_quantity,omitempty"`
	Grams                      int                  `json:"grams,omitempty"`
	FulfillmentStatus          string               `json:"fulfillment_status,omitempty"`
	TaxLines                   []TaxLine            `json:"tax_lines,omitempty"`
	AppliedDiscount            *AppliedDiscount     `json:"applied_discount"`
	DiscountAllocations        []DiscountAllocation `json:"discount_allocations,omitempty"`
}
type DiscountAllocation struct {
	Amount                   *decimal.Decimal `json:"amount,omitempty"`
	DiscountApplicationIndex int              `json:"discount_application_index,omitempty"`
}

type ShippingLines struct {
	ID                            int64            `json:"id,omitempty"`
	Title                         string           `json:"title,omitempty"`
	Price                         *decimal.Decimal `json:"price,omitempty"`
	Code                          string           `json:"code,omitempty"`
	Source                        string           `json:"source,omitempty"`
	Phone                         string           `json:"phone,omitempty"`
	RequestedFulfillmentServiceID string           `json:"requested_fulfillment_service_id,omitempty"`
	DeliveryCategory              string           `json:"delivery_category,omitempty"`
	CarrierIdentifier             string           `json:"carrier_identifier,omitempty"`
	TaxLines                      []TaxLine        `json:"tax_lines,omitempty"`
	Handle                        string           `json:"handle,omitempty"`
	Custom                        bool             `json:"custom,omitempty"`
}

type Transaction struct {
	ID                int64            `json:"id,omitempty"`
	OrderID           int64            `json:"order_id,omitempty"`
	Amount            *decimal.Decimal `json:"amount,omitempty"`
	Kind              string           `json:"kind,omitempty"`
	Gateway           string           `json:"gateway,omitempty"`
	Status            string           `json:"status,omitempty"`
	Message           string           `json:"message,omitempty"`
	CreatedAt         *time.Time       `json:"created_at,omitempty"`
	ProcessedAt       *time.Time       `json:"processed_at,omitempty"`
	Test              bool             `json:"test,omitempty"`
	Authorization     string           `json:"authorization,omitempty"`
	Currency          string           `json:"currency,omitempty"`
	LocationID        *int64           `json:"location_id,omitempty"`
	UserID            *int64           `json:"user_id,omitempty"`
	ParentID          int64            `json:"parent_id,omitempty"`
	DeviceID          *int64           `json:"device_id,omitempty"`
	ErrorCode         string           `json:"error_code,omitempty"`
	SourceName        string           `json:"source_name,omitempty"`
	Source            string           `json:"source,omitempty"`
	PaymentDetails    []string         `json:"payment_details,omitempty"`
	MaximumRefundable *decimal.Decimal `json:"maximum_refundable,omitempty"`
}

type Address struct {
	ID           int64   `json:"id,omitempty"`
	Address1     string  `json:"address1,omitempty"`
	Address2     string  `json:"address2,omitempty"`
	City         string  `json:"city,omitempty"`
	Company      string  `json:"company,omitempty"`
	Country      string  `json:"country,omitempty"`
	CountryCode  string  `json:"country_code,omitempty"`
	FirstName    string  `json:"first_name,omitempty"`
	LastName     string  `json:"last_name,omitempty"`
	Latitude     float64 `json:"latitude,omitempty"`
	Longitude    float64 `json:"longitude,omitempty"`
	Name         string  `json:"name,omitempty"`
	Phone        string  `json:"phone,omitempty"`
	Province     string  `json:"province,omitempty"`
	ProvinceCode string  `json:"province_code,omitempty"`
	Zip          string  `json:"zip,omitempty"`
	Email        string  `json:"email,omitempty"`
	LocaltionID  string  `json:"localtion_id,omitempty"`
}
type AppliedDiscount struct {
	Title       string `json:"title,omitempty"`
	Description string `json:"description,omitempty"`
	Value       string `json:"value,omitempty"`
	ValueType   string `json:"value_type,omitempty"`
	Amount      string `json:"amount,omitempty"`
}

type DiscountApplication struct {
	Type             string           `json:"type,omitempty"`
	Title            string           `json:"title,omitempty"`
	Description      string           `json:"description,omitempty"`
	Value            *decimal.Decimal `json:"value,omitempty"`
	ValueType        string           `json:"value_type,omitempty"`
	AllocationMethod string           `json:"allocation_method,omitempty"`
	TargetSelection  string           `json:"target_selection,omitempty"`
	TargetType       string           `json:"target_type,omitempty"`
	Amount           string           `json:"amount,omitempty"`
}

func (s *OrderServiceOp) Create(order Order) (*Order, error) {
	path := fmt.Sprintf("%s.json", ordersBasePath)
	wrappedData := OrderResource{Order: &order}
	resource := new(OrderResource)
	err := s.client.Post(path, wrappedData, resource)
	return resource.Order, err
}

func (s *OrderServiceOp) Get(orderID int64, options interface{}) (*Order, error) {
	path := fmt.Sprintf("%s/%d.json", ordersBasePath, orderID)
	resource := new(OrderResource)
	err := s.client.Get(path, resource, options)
	return resource.Order, err
}
