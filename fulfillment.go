package go_shopify

import "time"

type Fulfillment struct {
	ID         int64      `json:"id,omitempty"`
	OrderID    int64      `json:"order_id,omitempty"`
	LineItems  []LineItem `json:"line_items,omitempty"`
	LocationID int64      `json:"location_id,omitempty"`
	Status     string     `json:"status,omitempty"`
	CreatedAt  *time.Time `json:"created_at,omitempty"`
	Service    string     `json:"service,omitempty"`
	UpdatedAt  *time.Time `json:"updated_at,omitempty"`
}
