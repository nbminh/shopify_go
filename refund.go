package go_shopify

import (
    "fmt"
    "github.com/shopspring/decimal"
    "time"
)

const refundResourceName = "refunds"

type RefundService interface {
    Create(Refund, int) (*Refund, error)
    Calculate(Refund, int) (*Refund, error)
}

type RefundServiceOp struct {
    client *Client
}

type Refund struct {
    ID               int                     `json:"id,omitempty"`
    OrderID          int                     `json:"order_id,omitempty"`
    CreatedAt        *time.Time              `json:"created_at,omitempty"`
    ProcessedAt      *time.Time              `json:"processed_at,omitempty"`
    Note             string                  `json:"note,omitempty"`
    Restock          bool                    `json:"restock,omitempty"`
    Notify           bool                    `json:"notify,omitempty"`
    RefundLineItems  []RefundLineItem        `json:"refund_line_items,omitempty"`
    Transactions     []Transaction           `json:"transactions,omitempty"`
    OrderAdjustments []RefundOrderAdjustment `json:"order_adjustments,omitempty"`
    Shipping         RefundShipping          `json:"shipping,omitempty"`
}

type RefundLineItem struct {
    ID                      int              `json:"id,omitempty"`
    Quantity                int              `json:"quantity,omitempty"`
    RestockType             string           `json:"restock_type,omitempty"`
    LocationID              int              `json:"location_id,omitempty"`
    LineItemID              int              `json:"line_item_id,omitempty"`
    LineItem                *LineItem        `json:"line_item,omitempty"`
    Subtotal                *decimal.Decimal `json:"subtotal,omitempty"`
    TotalTax                *decimal.Decimal `json:"total_tax,omitempty"`
    DiscountedPrice         *decimal.Decimal `json:"discounted_price,omitempty"`
    DiscountedTotalPrice    *decimal.Decimal `json:"discounted_total_price,omitempty"`
    TotalCartDiscountAmount *decimal.Decimal `json:"total_cart_discount_amount,omitempty"`
    Price                   *decimal.Decimal `json:"price,omitempty"`
}

type RefundOrderAdjustment struct {
    ID           int              `json:"id,omitempty"`
    OrderID      int              `json:"order_id,omitempty"`
    RefundID     int              `json:"refund_id,omitempty"`
    Amount       *decimal.Decimal `json:"amount,omitempty"`
    TaxAmount    *decimal.Decimal `json:"tax_amount,omitempty"`
    Kind         string           `json:"kind,omitempty"`
    Reason       string           `json:"reason,omitempty"`
    AmountSet    AmountSet        `json:"amount_set,omitempty"`
    TaxAmountSet AmountSet        `json:"tax_amount_set,omitempty"`
}

type RefundShipping struct {
    FullRefund        bool             `json:"full_refund,omitempty"`
    Amount            *decimal.Decimal `json:"amount,omitempty"`
    Tax               *decimal.Decimal `json:"tax,omitempty"`
    MaximumRefundable *decimal.Decimal `json:"maximum_refundable,omitempty"`
}

type AmountSet struct {
    ShopMoney        ShopMoney        `json:"shop_money"`
    PresentmentMoney PresentmentMoney `json:"presentment_money"`
}

type ShopMoney struct {
    Amount       *decimal.Decimal `json:"amount"`
    CurrencyCode string           `json:"currency_code"`
}

type PresentmentMoney struct {
    Amount       *decimal.Decimal `json:"amount"`
    CurrencyCode string           `json:"currency_code"`
}

type RefundResource struct {
    Refund *Refund `json:"refund"`
}

type RefundsResource struct {
    Refunds []Refund `json:"refunds"`
}

func (s *RefundServiceOp) Create(refund Refund, orderID int) (*Refund, error) {
    path := fmt.Sprintf("%s/%d/%s.json", ordersBasePath, orderID, refundResourceName)
    wrappedData := RefundResource{Refund: &refund}
    resource := new(RefundResource)
    err := s.client.Post(path, wrappedData, resource)

    return resource.Refund, err
}

func (s *RefundServiceOp) Calculate(refund Refund, orderID int) (*Refund, error) {
    path := fmt.Sprintf("%s/%d/%s/calculate.json", ordersBasePath, orderID, refundResourceName)
    wrappedData := RefundResource{Refund: &refund}
    resource := new(RefundResource)
    err := s.client.Post(path, wrappedData, resource)

    return resource.Refund, err
}
