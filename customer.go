package go_shopify

import "time"

type CustomerOrder struct {
    ID        int64      `json:"id,omitempty"`
    FirstName string     `json:"first_name,omitempty"`
    LastName  string     `json:"last_name,omitempty"`
    Email     string     `json:"email,omitempty"`
    Phone     string     `json:"phone,omitempty"`
    Status    string     `json:"status,omitempty"`
    State     string     `json:"state,omitempty"`
    CreatedAt *time.Time `json:"created_at,omitempty"`
    UpdatedAt *time.Time `json:"updated_at,omitempty"`
}
