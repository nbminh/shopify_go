package go_shopify

import "fmt"

type TransactionService interface {
	List(int64, interface{}) ([]Transaction, error)
	Create(int64, Transaction) (*Transaction, error)
	Get(int64, int64, interface{}) (*Transaction, error)
}

type TransactionServiceOp struct {
	client *Client
}

type TransactionResource struct {
	Transaction *Transaction `json:"transaction"`
}

type TransactionsResource struct {
	Transactions []Transaction `json:"transactions"`
}

func (s *TransactionServiceOp) Create(orderID int64, transaction Transaction) (*Transaction, error) {
	path := fmt.Sprintf("%s/%d/transactions.json", ordersBasePath, orderID)
	wrappedData := TransactionResource{Transaction: &transaction}
	resource := new(TransactionResource)
	err := s.client.Post(path, wrappedData, resource)
	return resource.Transaction, err
}
func (s *TransactionServiceOp) Get(orderID int64, transactionID int64, options interface{}) (*Transaction, error) {
	path := fmt.Sprintf("%s/%d/transactions/%d.json", ordersBasePath, orderID, transactionID)
	resource := new(TransactionResource)
	err := s.client.Get(path, resource, options)
	return resource.Transaction, err
}
func (s *TransactionServiceOp) List(orderID int64, options interface{}) ([]Transaction, error) {
	path := fmt.Sprintf("%s/%d/transactions.json", ordersBasePath, orderID)
	resource := new(TransactionsResource)
	err := s.client.Get(path, resource, options)
	return resource.Transactions, err
}
