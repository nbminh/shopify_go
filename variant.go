package go_shopify

type Variant struct {
    ID        int64  `json:"id,omitempty"`
    ProductID int64  `json:"product_id,omitempty"`
    Title     string `json:"title,omitempty"`
    Sku       string `json:"sku"`
    Price     string `json:"price,omitempty"`
}
