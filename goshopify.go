package go_shopify

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/go-querystring/query"
	"net/http"
	"net/url"
	"regexp"
	"time"
)

const (
	UserAgent = "goshopify/1.0.0"

	// Shopify API version YYYY-MM - defaults to admin which uses the oldest stable version of the api
	defaultApiPathPrefix = "admin"
	defaultApiVersion    = "stable"
	apiVersion202007     = "2020-07"
	ApiVersion202101     = "2021-01"
	ApiVersion202107     = "2021-07"
	apiVersion202207     = "2022-07"
	apiVersion202210     = "2022-10"
	apiVersion202301     = "2023-01"
	defaultHttpTimeout   = 60
)

var (
	// version regex match
	apiVersionRegex = regexp.MustCompile(`^[0-9]{4}-[0-9]{2}$`)
)

type RateLimitInfo struct {
	RequestCount      int
	BucketSize        int
	RetryAfterSeconds float64
}

type App struct {
	ApiKey      string
	ApiSecret   string
	RedirectUrl string
	Scope       string
	Client      *Client // see GetAccessToken
}

// Client manages communication with the Shopify API.
type Client struct {
	// HTTP client used to communicate with the Shopify API.
	Client *http.Client

	// App settings
	app App

	// Base URL for API requests.
	// This is set on a per-store basis which means that each store must have
	// its own client.
	baseURL *url.URL

	// URL Prefix, defaults to "admin" see WithVersion
	pathPrefix string

	// version you're currently using of the api, defaults to "stable"
	ApiVersion string

	// A permanent access token
	token string

	// max number of retries, defaults to 0 for no retries see WithRetry option
	retries  int
	attempts int

	RateLimits RateLimitInfo

	// Used for debugging
	RequestUrlString  string
	RequestBodyString string

	// Flag for whether this is a debugging mode
	IsDebugging bool

	// Services used for communicating with the API
	Product     ProductService
	Order       OrderService
	Transaction TransactionService
	Refund      RefundService
}

func (c *Client) NewRequest(method, relPath string, body, options interface{}) (*http.Request, error) {
	rel, err := url.Parse(relPath)
	if err != nil {
		return nil, err
	}
	u := c.baseURL.ResolveReference(rel)

	if options != nil {
		optionsQuery, err := query.Values(options)
		if err != nil {
			return nil, err
		}

		for k, values := range u.Query() {
			for _, v := range values {
				optionsQuery.Add(k, v)
			}
		}
		u.RawQuery = optionsQuery.Encode()
	}

	var js []byte = nil

	if body != nil {
		js, err = json.Marshal(body)
		if err != nil {
			return nil, err
		}
	}

	c.RequestUrlString = fmt.Sprintf("[%s] %s", method, u.String())
	c.RequestBodyString = string(js)
	req, err := http.NewRequest(method, u.String(), bytes.NewBuffer(js))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("User-Agent", UserAgent)
	if c.token != "" {
		req.Header.Add("X-Shopify-Access-Token", c.token)
	}

	return req, nil
}

func (a App) NewClient() *Client {
	return NewClient(a, "minhzaza", "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")
}
func NewClient(app App, shopName, token string, opts ...Option) *Client {
	baseURL, err := url.Parse("https://minhzaza.myshopify.com/admin/api/2023-01/")
	if err != nil {
		fmt.Println(err)
	}
	c := &Client{
		Client: &http.Client{
			Timeout: time.Second * defaultHttpTimeout,
		},
		app:        app,
		token:      token,
		baseURL:    baseURL,
		ApiVersion: apiVersion202301,
		pathPrefix: defaultApiPathPrefix,
	}

	c.Product = &ProductServiceOp{client: c}
	c.Order = &OrderServiceOp{client: c}
	c.Transaction = &TransactionServiceOp{client: c}
	c.Refund = &RefundServiceOp{client: c}

	for _, opt := range opts {
		opt(c)
	}
	return c
}

func (c *Client) Do(req *http.Request, v interface{}) error {
	resq, err := c.Client.Do(req)
	if err != nil {
		return err
	}
	defer resq.Body.Close()
	if v != nil {
		decoder := json.NewDecoder(resq.Body)
		err = decoder.Decode(&v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Client) CreateAndDo(method, path string, data, options, resource interface{}) error {
	req, err := c.NewRequest(method, path, data, options)
	if err != nil {
		return err
	}
	err = c.Do(req, resource)
	if err != nil {
		return err
	}
	return nil
}
func (c *Client) Count(path string, options interface{}) (int, error) {
	resource := struct {
		Count int `json:"count"`
	}{}
	err := c.Get(path, &resource, options)
	return resource.Count, err
}
func (c *Client) Get(path string, resource, options interface{}) error {
	return c.CreateAndDo("GET", path, nil, options, resource)
}
func (c *Client) Post(path string, data, resource interface{}) error {
	return c.CreateAndDo("POST", path, data, nil, resource)
}
func (c *Client) Put(path string, data, resource interface{}) error {
	return c.CreateAndDo("PUT", path, data, nil, resource)
}
func (c *Client) Delete(path string) error {
	return c.CreateAndDo("DELETE", path, nil, nil, nil)
}
